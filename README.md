**Bleb Resources - Race For The Wool**

To test the latest development version of this pack, [download the source as a zip](https://gitlab.com/blebmc/blebtex-rftw/-/archive/master/blebtex-rftw-master.zip) and add it to your Minecraft instance with your other resource packs.


Stable releases are here: [https://www.curseforge.com/minecraft/texture-packs/blebtex-rftw](https://www.curseforge.com/minecraft/texture-packs/blebtex-rftw)
